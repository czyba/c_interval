
/// Performs addition that returns `None` instead of wrapping around on
/// overflow.
pub trait CheckedAdd<A>: Sized {
    /// Adds two numbers, checking for overflow. If overflow happens, `None` is
    /// returned.
    fn checked_add(self, v: A) -> Option<Self>;
}

macro_rules! checked_add_impl {
    ($t:ty) => {
        impl CheckedAdd<$t> for $t {
            #[inline]
            fn checked_add(self, v: $t) -> Option<$t> {
                <$t>::checked_add(self, v)
            }
        }
    };
}

checked_add_impl!(u8);
checked_add_impl!(u16);
checked_add_impl!(u32);
checked_add_impl!(u64);
checked_add_impl!(usize);
#[cfg(has_i128)]
checked_add_impl!(u128);

checked_add_impl!(i8);
checked_add_impl!(i16);
checked_add_impl!(i32);
checked_add_impl!(i64);
checked_add_impl!(isize);
#[cfg(has_i128)]
checked_add_impl!(i128);
