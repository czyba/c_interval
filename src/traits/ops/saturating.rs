
/// Performs addition that returns `None` instead of wrapping around on
/// overflow.
pub trait SaturatingAdd<A>: Sized {
    /// Adds two numbers, checking for overflow. If overflow happens, `None` is
    /// returned.
    fn saturating_add(self, v: A) -> Self;
}

macro_rules! saturating_add_impl {
    ($t:ty) => {
        impl SaturatingAdd<$t> for $t {
            #[inline]
            fn saturating_add(self, v: $t) -> Self {
                <$t>::saturating_add(self, v)
            }
        }
    };
}

saturating_add_impl!(u8);
saturating_add_impl!(u16);
saturating_add_impl!(u32);
saturating_add_impl!(u64);
saturating_add_impl!(usize);
#[cfg(has_i128)]
saturating_add_impl!(u128);

saturating_add_impl!(i8);
saturating_add_impl!(i16);
saturating_add_impl!(i32);
saturating_add_impl!(i64);
saturating_add_impl!(isize);
#[cfg(has_i128)]
saturating_add_impl!(i128);
