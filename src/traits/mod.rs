//! Provides various traits that can be implemented in order to create safe
//! intervals.
//!
mod ops;

pub use ops::*;
