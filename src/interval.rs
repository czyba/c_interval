//! Generic interval including their lower bound and excluding their upper bound.
//!
//! Let `D` be a domain with a total order. Given two value `lb, ub ∈ D` the
//! interval `I` denotes the set of values defined as `I := { x ∈ D | lb <= x < ub }`,
//! denoted as `[lb, ub)`.
//!
//! Thus, any type implementing [`Ord`] is elligable to create an interval.
//!
//! Depending on the traits the bound type implements several arithmetic
//! operations are supported as well,
//!
//! [`Ord`]: https://doc.rust-lang.org/std/cmp/trait.Ord.html
use std::cmp;
use std::fmt;

/// Represents an interval on a given domain type. The lower bound is inclusive,
/// the upper is exclusive.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub struct Interval<T> {
    lower_bound: T,
    upper_bound: T,
}

impl<T> Interval<T> {

    /// Creates a new interval from a given lower bound and upper bound. May
    /// result in an empty interval if `lower_bound >= upper_bound`.
    ///
    /// # Example
    ///
    /// Basic Usage:
    ///
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let interval = Interval::new(5, 5);
    ///
    /// // empty interval
    /// assert!(interval.is_empty());
    ///
    /// let upper_bound = 6;
    /// let interval = Interval::new(5, 6);
    ///
    /// // Valid Interval
    /// assert_eq!(5, *interval.get_lower_bound());
    /// assert_eq!(6, *interval.get_upper_bound());
    /// ```
    pub const fn new(lower_bound: T, upper_bound: T) -> Interval<T>
    {
        Interval {
            lower_bound: lower_bound,
            upper_bound: upper_bound,
        }
    }

    /// Returns the lower bound of the interval.
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let interval = Interval::new(5, 6);
    ///
    /// assert_eq!(&5, interval.get_lower_bound());
    /// ```
    pub fn get_lower_bound(&self) -> &T {
        &self.lower_bound
    }

    /// Returns the upper bound of the interval.
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let interval = Interval::new(5, 6);
    ///
    /// assert_eq!(&6, interval.get_upper_bound());
    /// ```
    pub fn get_upper_bound(&self) -> &T {
        &self.upper_bound
    }
}

impl<T> Interval<T>
where
    T: Ord,
{

    /// Returns whether or not this interval contains any values
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let empty = Interval::new(5, 5);
    /// assert!(empty.is_empty());
    ///
    /// let nonempty = Interval::new(5, 6);
    /// assert!(!nonempty.is_empty());
    /// ```
    pub fn is_empty(&self) -> bool {
        self.lower_bound >= self.upper_bound
    }

    /// Checks if `value` is contained in the [`Interval`].
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// // Create interval [5,6)
    /// let interval = Interval::new(5, 6);
    ///
    /// // 4 is not part of that interval
    /// assert!(!interval.contains_value(&4));
    ///
    /// // 5 is part of that interval
    /// assert!(interval.contains_value(&5));
    ///
    /// // 6 is no longer part of that interval
    /// assert!(!interval.contains_value(&6));
    /// ```
    ///
    /// [`Interval`]: struct.Interval.html
    pub fn contains_value(&self, value: &T) -> bool {
        &self.lower_bound <= value && value < &self.upper_bound
    }

    /// Checks if self contains_interval the complete [`Interval`] `containee`.
    /// An empty interval contains only the empty interval, the empty interval
    /// is contained by every interval.
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let encloser = Interval::new(0, 10);
    /// let enclosee = Interval::new(0, 9);
    ///
    /// // [0,9) is a subset of [0, 10)
    /// assert!(encloser.contains_interval(&enclosee));
    ///
    /// // [0,10) is also a subset of [0, 10)
    /// assert!(encloser.contains_interval(&encloser));
    ///
    /// // [0,10) is not a subset of [0, 9)
    /// assert!(!enclosee.contains_interval(&encloser));
    /// ```
    ///
    /// [`Interval`]: struct.Interval.html
    pub fn contains_interval(&self, enclosee: &Self) -> bool {
        if enclosee.is_empty() {
            return true;
        }
        if self.is_empty() {
            return false;
        }
        self.lower_bound <= enclosee.lower_bound && enclosee.upper_bound <= self.upper_bound
    }

    /// Checks if self intersects with the given other [`Interval`].
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let interval1 = Interval::new(0, 2);
    /// let interval2 = Interval::new(1, 3);
    /// let interval3 = Interval::new(2, 4);
    ///
    /// // interval1 intersects interval2
    /// assert!(interval1.intersects(&interval2));
    ///
    /// // interval1 does not intersect interval3 however
    /// assert!(!interval1.intersects(&interval3));
    /// ```
    ///
    /// [`Interval`]: struct.Interval.html
    pub fn intersects(&self, other: &Self) -> bool {
        if self.is_empty() || other.is_empty() {
            return false;
        }
        other.lower_bound < self.upper_bound && self.lower_bound < other.upper_bound
    }

    /// Intersects `self` with another [`Interval`].
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let interval1 = Interval::new(0, 2);
    /// let interval2 = Interval::new(1, 3);
    /// let interval3 = Interval::new(2, 4);
    ///
    /// let expected_1_2 = Interval::new(1, 2);
    ///
    /// // There is an intersection of [1,2) between interval1 and interval2
    /// assert_eq!(expected_1_2, interval1.intersection(&interval2));
    ///
    /// // There is no intersection between interval1 and interval3 however
    /// assert!(interval1.intersection(&interval3).is_empty());
    /// ```
    ///
    /// [`Interval`]: struct.Interval.html
    pub fn intersection(&self, other: &Self) -> Self
    where
        T: Clone,
    {
        Self::new(
            self.lower_bound.clone().max(other.lower_bound.clone()),
            self.upper_bound.clone().min(other.upper_bound.clone()),
        )
    }

    /// Tests whether between `self` and the other [`Interval`] exists a gap.
    /// If either argument is empty, the result will be `false`.
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let interval1 = Interval::new(0, 2);
    /// let interval2 = Interval::new(2, 3);
    /// let interval3 = Interval::new(3, 4);
    ///
    /// // There is no gap between interval1 and interval2
    /// assert!(!interval1.has_gap(&interval2));
    /// assert!(!interval2.has_gap(&interval1));
    ///
    /// // There is a gap of [2,3] between interval1 and interval3
    /// assert!(interval1.has_gap(&interval3));
    /// assert!(interval3.has_gap(&interval1));
    /// ```
    ///
    /// [`Interval`]: struct.Interval.html
    pub fn has_gap(&self, other: &Self) -> bool
    {
        if self.is_empty() || other.is_empty() {
            return false;
        }
        other.upper_bound < self.lower_bound || self.upper_bound < other.lower_bound
    }

    /// Calculates the gap interval between `self` and another [`Interval`].
    /// If either interval is empty, an empty interval will be returned.
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let interval1 = Interval::new(0, 2);
    /// let interval2 = Interval::new(1, 4);
    /// let interval3 = Interval::new(3, 5);
    ///
    /// let expected_1_3 = Interval::new(2, 3);
    ///
    /// // There is no gap betwen interval1 and interval2
    /// assert!(interval1.gap(&interval2).is_empty());
    ///
    /// // There is a gap of [2,3) between interval1 and interval3, however
    /// assert_eq!(expected_1_3, interval1.gap(&interval3));
    /// ```
    ///
    /// [`Interval`]: struct.Interval.html
    pub fn gap(&self, other: &Self) -> Self
    where
        T: Clone,
    {
        if self.is_empty() {
            return self.clone();
        }
        if other.is_empty() {
            return other.clone();
        }
        Self::new(
            cmp::min(self.upper_bound.clone(), other.upper_bound.clone()),
            cmp::max(self.lower_bound.clone(), other.lower_bound.clone()),
        )
    }

    /// Returns the least upper bound between self and the given [`Interval`].
    ///
    /// The operation is defined as: `hull([lb1, ub1], [lb2, ub2]) := [min(lb1, lb2), max(ub1, ub2)]`
    ///
    /// Empty intervals are ignored during the calculation. If both are empty, an
    /// empty interval will be returned instead.
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let interval1 = Interval::new(0, 1);
    /// let interval2 = Interval::new(3, 4);
    ///
    /// let expected_interval = Interval::new(0, 4);
    ///
    /// assert_eq!(expected_interval, interval1.hull(&interval2));
    /// ```
    ///
    /// [`Interval`]: struct.Interval.html
    pub fn hull(&self, other: &Self) -> Self
    where
        T: Clone
    {
        if self.is_empty() {
            return other.clone();
        }
        if other.is_empty() {
            return self.clone();
        }
        Self {
            lower_bound: cmp::min(self.lower_bound.clone(), other.lower_bound.clone()),
            upper_bound: cmp::max(self.upper_bound.clone(), other.upper_bound.clone()),
        }
    }

    /// Unchecked addition of a constant to an interval. If an overflow occurs
    /// on only a single bound, the result will be an empty interval. If both
    /// overflow the interval will be non-empty again.
    ///
    /// If the interval is empty, the result will be empty as well.
    ///
    /// # Examples
    ///
    /// Basic Usage:
    ///
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let interval = Interval::new(100u8, 200u8);
    ///
    /// let interval = interval.unchecked_add(50);
    ///
    /// let expected = Interval::new(150u8, 250u8);
    ///
    /// assert_eq!(expected, interval);
    /// ```
    ///
    /// This will result in an empty interval:
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let interval = Interval::new(100u8, 200u8);
    ///
    /// # #[cfg(not(debug_assertions))]
    /// assert!(interval.unchecked_add(100u8).is_empty());
    /// ```
    ///
    /// An empty interval will always result in another empty interval
    pub fn unchecked_add<A>(self, rhs: A) -> Self
    where
        T: std::ops::Add<A, Output = T>,
        A: Clone
    {
        if self.is_empty() {
            return self;
        }
        Self::new(
            self.lower_bound + rhs.clone(),
            self.upper_bound + rhs,
        )
    }

    /// Checked addition of interval to constant. If the operation would
    /// result in any kind of overflow, [`None`] is returned.
    ///
    /// If the interval is empty, the result will be empty as well.
    ///
    /// # Examples
    ///
    /// Basic Usage:
    ///
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let interval = Interval::new(100u8, 200u8);
    ///
    /// let interval = interval.checked_add(50);
    ///
    /// let expected = Interval::new(150u8, 250u8);
    /// assert_eq!(Some(expected), interval);
    ///
    /// let interval = interval.unwrap().checked_add(50);
    /// assert_eq!(None, interval);
    /// ```
    ///
    /// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
    pub fn checked_add<A>(self, rhs: A) -> Option<Self>
    where
        T: crate::traits::CheckedAdd<A>,
        A: Clone
    {
        if self.is_empty() {
            return Some(self);
        }
        let lower_bound = self.lower_bound.checked_add(rhs.clone())?;
        let upper_bound = self.upper_bound.checked_add(rhs)?;

        Some(Self::new(
            lower_bound,
            upper_bound,
        ))
    }

    /// Saturated addition of interval to constant. If
    /// the upper bound overflows it will be rounded to its highest allowed
    /// value. If the lower bound underflows it will be rounded to its lowest
    /// allowed value.
    ///
    /// If the interval is empty, the result will be empty as well.
    ///
    /// # Examples
    ///
    /// Basic Usage:
    ///
    /// ```
    /// # use interval_container_library::interval::Interval;
    /// let interval = Interval::new(100u8, 200u8);
    ///
    /// // Upper bound overflows, thus results in [200, 255]
    /// let interval = interval.saturating_add(100);
    ///
    /// let expected = Interval::new(200u8, 255u8);
    /// assert_eq!(expected, interval);
    ///
    /// // Both bounds overflow => resulting in the empty interval (255,255]
    /// let interval = interval.saturating_add(100);
    ///
    /// assert!(interval.is_empty());
    /// ```
    pub fn saturating_add<A>(self, rhs: A) -> Self
    where
        T: crate::traits::SaturatingAdd<A>,
        A: Clone
    {
        let lower_bound = self.lower_bound.saturating_add(rhs.clone());
        let upper_bound = self.upper_bound.saturating_add(rhs);
        Self::new(lower_bound, upper_bound)
    }
}

impl<T> fmt::Display for Interval<T>
where
    T: fmt::Display + Ord,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.is_empty() {
            write!(f, "[)")
        } else {
            write!(f, "[{}, {})", self.lower_bound, self.upper_bound)
        }
    }
}

impl<T> std::convert::From<(T,T)> for Interval<T>
where
    T: Ord
{
    fn from(pair: (T,T)) -> Interval<T> {
        Interval::new(pair.0, pair.1)
    }
}

impl<T, A> std::ops::Add<A> for Interval<T>
where
    T: cmp::Ord + std::ops::Add<A, Output = T>,
    A: Clone,
{
    type Output = Self;

    fn add(self, rhs: A) -> Self::Output {
        self.unchecked_add(rhs)
    }
}

impl<T, A> crate::traits::CheckedAdd<A> for Interval<T>
where
    T: std::cmp::Ord + crate::traits::CheckedAdd<A>,
    A: Clone
{
    fn checked_add(self, rhs: A) -> Option<Self> {
        Interval::checked_add(self, rhs)
    }
}

impl<T, A> crate::traits::SaturatingAdd<A> for Interval<T>
where
    T: std::cmp::Ord + crate::traits::SaturatingAdd<A>,
    A: Clone
{
    fn saturating_add(self, rhs: A) -> Self {
        Interval::saturating_add(self, rhs)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const EMPTY: Interval<i32> = Interval::new(0, 0);
    const I100_200: Interval<i32> = Interval::new(100, 200);

    mod new {

        use super::*;

        #[test]
        fn empty() {
            let val = Interval::new(100, 100);
            assert!(val.is_empty());
        }

        #[test]
        fn non_empty() {
            let val = Interval::new(100, 200);
            assert!(!val.is_empty());
        }
    }

    mod from_pair {
        use super::*;

        #[test]
        fn empty() {
            assert!(EMPTY.is_empty());
        }

        #[test]
        fn non_empty() {
            assert!(!I100_200.is_empty());
        }
    }

    mod contains_value {
        use super::*;
        #[test]
        fn not_contained() {
            assert!(!I100_200.contains_value(&50));
        }

        #[test]
        fn contained() {
            assert!(I100_200.contains_value(&150));
        }
    }

    mod contains_interval {
        use super::*;

        #[test]
        fn not_contained() {
            let not_enclosed = Interval::new(100, 201);
            assert!(!I100_200.contains_interval(&not_enclosed));
        }

        #[test]
        fn contained() {
            assert!(I100_200.contains_interval(&I100_200));
        }

        #[test]
        fn container_empty() {
            assert!(!EMPTY.contains_interval(&I100_200));
        }

        #[test]
        fn containee_empty() {
            assert!(I100_200.contains_interval(&EMPTY));
        }

        #[test]
        fn both_empty() {
            assert!(EMPTY.contains_interval(&EMPTY));
        }
    }

    mod intersects {
        use super::*;

        #[test]
        fn no_intersection() {
            let other = Interval::new(50,100);
            assert!(!I100_200.intersects(&other));
            let other = Interval::new(200, 250);
            assert!(!I100_200.intersects(&other));
        }

        #[test]
        fn intersection() {
            let other = Interval::new(51,101);
            assert!(I100_200.intersects(&other));
            let other = Interval::new(199, 250);
            assert!(I100_200.intersects(&other));
            let other = Interval::new(101, 199);
            assert!(I100_200.intersects(&other));
        }

        #[test]
        fn self_empty() {
            assert!(!EMPTY.intersects(&I100_200));
        }

        #[test]
        fn other_empty() {
            assert!(!I100_200.intersects(&EMPTY));
        }
    }

    mod intersection {
        use super::*;

        #[test]
        fn no_intersection() {
            let other = Interval::new(50,100);
            assert!(I100_200.intersection(&other).is_empty());
            let other = Interval::new(200,300);
            assert!(I100_200.intersection(&other).is_empty());
        }

        #[test]
        fn intersection() {
            let other = Interval::new(51,101);
            let expected = Interval::new(100,101);
            assert_eq!(expected, I100_200.intersection(&other));
            let other = Interval::new(199,300);
            let expected = Interval::new(199,200);
            assert_eq!(expected, I100_200.intersection(&other));
        }

        #[test]
        fn self_empty() {
            assert!(EMPTY.intersection(&I100_200).is_empty());
        }

        #[test]
        fn other_empty() {
            assert!(I100_200.intersection(&EMPTY).is_empty());
        }
    }

    mod has_gap {
        use super::*;

        #[test]
        fn no_gap() {
            let other = Interval::new(0,100);
            assert!(!I100_200.has_gap(&other));
            let other = Interval::new(200,300);
            assert!(!I100_200.has_gap(&other));
        }

        #[test]
        fn gap() {
            let other = Interval::new(0,99);
            assert!(I100_200.has_gap(&other));
            let other = Interval::new(201,300);
            assert!(I100_200.has_gap(&other));
        }

        #[test]
        fn self_empty() {
            assert!(!EMPTY.has_gap(&I100_200));
        }

        #[test]
        fn other_empty() {
            assert!(!I100_200.has_gap(&EMPTY));
        }
    }

    mod gap {
        use super::*;

        #[test]
        fn gap_no_gap() {
            let other = Interval::new(0,100);
            assert!(I100_200.gap(&other).is_empty());
            let other = Interval::new(200,300);
            assert!(I100_200.gap(&other).is_empty());
        }

        #[test]
        fn gap() {
            let other = Interval::new(0,99);
            let expected = Interval::new(99,100);
            assert_eq!(expected, I100_200.gap(&other));
            let other = Interval::new(201,300);
            let expected = Interval::new(200,201);
            assert_eq!(expected, I100_200.gap(&other));
        }

        #[test]
        fn self_empty() {
            assert!(EMPTY.gap(&I100_200).is_empty())
        }

        #[test]
        fn other_empty() {
            assert!(I100_200.gap(&EMPTY).is_empty())
        }
    }

    mod hull {

        use super::*;

        #[test]
        fn hull() {
            let val1 = Interval::new(1000,1337);
            let val2 = Interval::new(77,777);
            let expected = Interval::new(77,1337);
            assert_eq!(expected, val1.hull(&val2));
        }

        #[test]
        fn self_empty() {
            assert_eq!(I100_200, EMPTY.hull(&I100_200));
        }

        #[test]
        fn other_empty() {
            assert_eq!(I100_200, I100_200.hull(&EMPTY));
        }
    }

    mod unchecked_add {
        use super::*;

        #[test]
        fn unchecked_add() {
            let interval = I100_200.clone();
            let interval = interval.unchecked_add(10);
            let expected = Interval::new(110, 210);
            assert_eq!(expected, interval);
        }

        // In debug this panics, because overflows panic
        #[cfg(debug_assertions)]
        #[should_panic]
        #[test]
        fn overflow_debug_panic() {
            let interval = Interval::new(100u8, 200);
            let result = interval.unchecked_add(100);
            assert!(result.is_empty());
        }

        #[cfg(not(debug_assertions))]
        #[test]
        fn overflow_empty() {
            let interval = Interval::new(100u8, 200);
            let result = interval.unchecked_add(100);
            assert!(result.is_empty());
        }

        #[test]
        fn empty_add() {
            assert_eq!(EMPTY, EMPTY.clone().unchecked_add(100));
        }
    }

    mod checked_add {

        use super::*;

        #[test]
        fn checked_add() {
            let interval = I100_200.clone().checked_add(10);
            let expected = Interval::new(110, 210);
            assert_eq!(Some(expected), interval);
        }

        #[test]
        fn overflow() {
            let interval = Interval::new(-100i8, 100i8);
            let interval = interval.checked_add(30);
            assert_eq!(None, interval);
        }

        #[test]
        fn empty() {
            let result = EMPTY.clone().checked_add(100).unwrap();
            assert!(result.is_empty());
        }
    }

    mod saturating_add {

        use super::*;

        #[test]
        fn saturating_add() {
            let interval = I100_200.clone().saturating_add(10);
            let expected = Interval::new(110, 210);
            assert_eq!(expected, interval);
        }

        #[test]
        fn saturated_underflow_stays_at_bound() {
            let interval = Interval::new(-100i8, 100i8);
            let interval = interval.saturating_add(-30);
            let expected = Interval::new(-128, 70);
            assert_eq!(expected, interval);
        }

        #[test]
        fn saturated_overflow_stays_at_bound() {
            let interval = Interval::new(-100i8, 100i8);
            let interval = interval.saturating_add(30);
            let expected = Interval::new(-70,127);
            assert_eq!(expected, interval);
        }

        #[test]
        fn saturated_empty_if_both_at_bound() {
            let interval = Interval::new(100i8,101i8);
            let interval = interval.saturating_add(30);
            assert!(interval.is_empty());
        }

        #[test]
        fn empty() {
            let result = EMPTY.clone().saturating_add(100);
            assert!(result.is_empty());
        }
    }

    mod display {
        use super::*;

        #[test]
        fn nonempty() {
            use std::fmt::Write as FmtWrite;
            let mut s = String::new();
            write!(&mut s, "{}", I100_200).unwrap();

            assert_eq!("[100, 200)", &s);
        }

        #[test]
        fn empty() {
            use std::fmt::Write as FmtWrite;
            let mut s = String::new();
            write!(&mut s, "{}", EMPTY).unwrap();

            assert_eq!("[)", &s);
        }
    }
}
