//! A library providing generic classes for interval arithmetic for different
//! Types. Also provides collections.
//!
//! # Interval
//! In order to use Interval the TypeParameter is required to implement [`Ord`].
//! We may relax this requirement to [`PartialOrd`] in the future.
//!
//! # Overflows
//! Since most types do not implement [`Add`] or other
//! arithmetic operations in an overflow safe manner, adding `100` to an
//! interval of `[100,200)` of type [`u8`], will result in an overflow for the
//! upper bound but not for the lower bound. Thus it results in an empty
//! interval.
//!
//! We provide checked operations, which will detect over- and underflows and
//! return an error, and saturating operations, which return the highest or
//! lowest allowed value in case of over- and underflows. In order to provide
//! these types the domain type will need to implement the respective traits.
//!
//! # Performance
//! The following tables summarize the performance for the different opreations
//! of the data types provided by this crate.
//!
//! If an entry is marked with a `-`, the operation has not been implemented
//! yet. If an operation is marked with `x`, there is currently no plan on
//! supporting the operation at this point in time.
//!
//! Note on [`IntervalSet`]: The implementation is currently backed by a vector.
//! Therefore, certain operations may require a longer time to complete.
//! Unfortunately, the current standard library's set implementation does not
//! provide the methods required to efficiently use them to implement the
//! interval set.
//!
//! ## Containedness
//! |                 | contains_value | contains_interval |
//! |-----------------|----------------|-------------------|
//! | [`Interval`]    | O(1)           | O(1)              |
//! | [`IntervalSet`] | O(log(n))      | O(log(n))         |
//!
//! ## Insertion
//! |                 | insert_interval |
//! |-----------------|-----------------|
//! | [`IntervalSet`] | O(n)            |
//!
//! ## Intersection
//! |                 | intersects_interval | intersection_interval |
//! |-----------------|---------------------|-----------------------|
//! | [`Interval`]    | O(1)                | O(1)                  |
//! | [`IntervalSet`] | O(log(n))           | O(n)                  |
//!
//! [`Interval`]: interval/struct.Interval.html
//! [`IntervalSet`]: interval_set/struct.IntervalSet.html
//! [`u8`]: https://doc.rust-lang.org/std/primitive.u8.html
//! [`Ord`]: https://doc.rust-lang.org/std/cmp/trait.Ord.html
//! [`PartialOrd`]: https://doc.rust-lang.org/std/cmp/trait.PartialOrd.html
//! [`Add`]: https://doc.rust-lang.org/std/ops/trait.Add.html
pub mod interval;
pub mod interval_set;
pub mod traits;
