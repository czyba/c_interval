//! Generic interval sets, merging intervals for smallest possible representation.
use std::vec::Vec;
use crate::interval::Interval;

/// Generic interval set. Merges all intersecting and abutting [`Interval`]s
/// in order to get the smallest possible representation.
///
/// [`Interval`]: ../interval/struct.Interval.html
#[derive(Debug, PartialEq, Eq)]
pub struct IntervalSet<T> {
    intervals: std::vec::Vec<Interval<T>>,
}

impl<T> IntervalSet<T>
{
    /// Creates a new empty interval set
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval_set::IntervalSet;
    /// let interval_set = IntervalSet::<i32>::new();
    ///
    /// assert!(interval_set.is_empty());
    /// ```
    pub fn new() -> IntervalSet<T> {
        IntervalSet {
            intervals: Vec::new(),
        }
    }

    fn with_capacity(capacity: usize) -> IntervalSet<T> {
        IntervalSet {
            intervals: Vec::with_capacity(capacity),
        }
    }

    /// Returns `true` if the interval set is empty, `false` otherwise.
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval_set::IntervalSet;
    /// let interval_set = IntervalSet::<i32>::new();
    ///
    /// assert!(interval_set.is_empty());
    /// ```
    pub fn is_empty(&self) -> bool {
        self.intervals.is_empty()
    }

    /// Returns an sorted iterator over all intervals.
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval_set::IntervalSet;
    /// # use interval_container_library::interval::Interval;
    /// let mut interval_set = IntervalSet::new();
    /// let i = Interval::new(0,5);
    /// interval_set.insert(i.clone());
    ///
    /// let mut iter = interval_set.iter();
    /// assert_eq!(Some(&i), iter.next());
    /// assert_eq!(None, iter.next());
    /// ```
    pub fn iter(&self) -> std::slice::Iter<Interval<T>> {
        self.intervals.iter()
    }
}

impl<T> IntervalSet<T>
where
    T: Ord
{
    /// Returns whether `self` contains the given value.
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval_set::IntervalSet;
    /// # use interval_container_library::interval::Interval;
    /// let mut interval_set = IntervalSet::new();
    /// interval_set.insert(Interval::new(0,2));
    /// interval_set.insert(Interval::new(6,10));
    ///
    /// assert!(interval_set.contains_value(&7));
    /// assert!(!interval_set.contains_value(&4));
    /// ```
    pub fn contains_value(&self, value: &T) -> bool {
        let potential_index = Self::find_potential_contained_in_index(&self.intervals, value);
        Self::check_value_contained_in_interval_at(&self.intervals, potential_index, value)
    }

    fn find_potential_contained_in_index(intervals: &[Interval<T>], value: &T) -> usize {
        let index = intervals.binary_search_by_key(
            &value,
            |probe| probe.get_upper_bound()
        );
        index.unwrap_or_else(|index| index)
    }

    fn check_value_contained_in_interval_at(intervals: &[Interval<T>], index: usize, value: &T) -> bool {
        let potential_interval = intervals.get(index);
        match potential_interval {
            Some(interval) => interval.contains_value(value),
            None => false,
        }
    }

    /// Returns whether `self` contains the given [`Interval`] completely.
    ///
    /// [`Interval`]: ../interval/struct.Interval.html
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval_set::IntervalSet;
    /// # use interval_container_library::interval::Interval;
    /// let mut interval_set = IntervalSet::new();
    /// interval_set.insert(Interval::new(0,5));
    /// interval_set.insert(Interval::new(6,10));
    ///
    /// assert!(interval_set.contains_interval(&Interval::new(7, 10)));
    /// assert!(!interval_set.contains_interval(&Interval::new(0, 10)));
    /// ```
    pub fn contains_interval(&self, interval: &Interval<T>) -> bool {
        let index = Self::find_intersection_from_index(&self.intervals, &interval);
        // If the interval is contained, it must be contained in the first relevant interval
        // Therefore we do not need to check the to_index for intersection.
        Self::check_interval_contained_in_interval_at(&self.intervals, index, interval)
    }

    fn check_interval_contained_in_interval_at(intervals: &[Interval<T>], index: usize, possibly_contained: &Interval<T>) -> bool {
        let potential_interval = intervals.get(index);
        match potential_interval {
            Some(interval) => interval.contains_interval(possibly_contained),
            None => possibly_contained.is_empty(), // Empty is always contained
        }
    }

    /// Inserts the given [`Interval`] into `self`,  merging all intervals that
    /// overlap or abut each other.
    ///
    /// [`Interval`]: ../interval/struct.Interval.html
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval_set::IntervalSet;
    /// # use interval_container_library::interval::Interval;
    /// let mut interval_set = IntervalSet::new();
    /// interval_set.insert(Interval::new(0,5));
    /// interval_set.insert(Interval::new(5,10));
    ///
    /// let mut iterator = interval_set.iter();
    /// assert_eq!(Some(&Interval::new(0,10)), iterator.next());
    /// assert_eq!(None, iterator.next());
    /// ```
    pub fn insert(&mut self, interval: Interval<T>)
    where
        T: Clone
    {
        let (delete_from, delete_to) = Self::find_delete_range(&self.intervals, &interval);
        let deleted_intervals = Self::delete_intervals(&mut self.intervals, delete_from, delete_to);
        let merged_interval = Self::merge_intervals(interval, deleted_intervals);
        Self::insert_nonempty_interval_at(&mut self.intervals, delete_from, merged_interval);
    }

    fn find_delete_range(intervals: &Vec<Interval<T>>, interval: &Interval<T>) -> (usize, usize) {
        let delete_from = Self::find_delete_from_index(intervals, interval);
        let delete_to = Self::find_delete_to_index(intervals, interval);
        (delete_from, delete_to)
    }

    fn find_delete_from_index(intervals: &Vec<Interval<T>>, interval: &Interval<T>) -> usize {
        let from_index = intervals.binary_search_by_key(
            &interval.get_lower_bound(),
            |probe| probe.get_upper_bound()
        );

        from_index.unwrap_or_else(|insert_point| insert_point)
    }

    fn find_delete_to_index(intervals: &Vec<Interval<T>>, interval: &Interval<T>) -> usize {
        let ub_insert_point = intervals.binary_search_by_key(
            &interval.get_upper_bound(),
            |probe| probe.get_lower_bound()
        );

        match ub_insert_point {
            // If we matched an ok, we include the found element as well, since
            // it abuts the current element. Since to "to_index" is excluding
            // we need to increment it once.
            Ok(insert_point) => insert_point + 1,
            Err(insert_point) => insert_point,
        }
    }

    fn delete_intervals(intervals: &mut Vec<Interval<T>>, delete_from: usize, delete_to: usize) -> Option<impl Iterator<Item=Interval<T>> + '_>{
        if delete_from < intervals.len() // deleting after the is after length, nothing to remove
            && delete_from < delete_to { // delete_to is exclusive, this implies an empty range to delete
            Some(intervals.drain(delete_from..delete_to))
        } else {
            None
        }
    }

    /// Merges all intervals into a single big interval.
    /// Assumes that the intervals of the iterator are ordered.
    fn merge_intervals<I> (interval: Interval<T>, iterator: Option<I>) -> Interval<T>
    where
        T: Clone,
        I: Iterator<Item=Interval<T>>,
    {
        let mut iterator = match iterator {
            Some(iterator) => iterator,
            None => return interval,
        };

        // Since we assume that the intervals are ordered we only need to merge
        // with the first and last interval.
        let first = iterator.next();
        let interval = match first {
            Some(first_interval) => interval.hull(&first_interval),
            None => return interval, // Early return since iterator is empty
        };

        let last = iterator.last();
        match last {
            Some(last_interval) => interval.hull(&last_interval),
            None => interval,
        }
    }

    fn insert_nonempty_interval_at(intervals: &mut Vec<Interval<T>>, insertion_point: usize, interval: Interval<T>) {
        if interval.is_empty() {
            return;
        }
        intervals.insert(insertion_point, interval);
    }

    /// Checks if `self` and the given [`Interval`] intersect each other.
    ///
    /// [`Interval`]: ../interval/struct.Interval.html
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval_set::IntervalSet;
    /// # use interval_container_library::interval::Interval;
    ///
    /// let mut interval_set = IntervalSet::new();
    /// interval_set.insert(Interval::new(0,2));
    /// interval_set.insert(Interval::new(7,10));
    ///
    /// assert!(interval_set.intersects_interval(&Interval::new(1,8)));
    /// assert!(!interval_set.intersects_interval(&Interval::new(2,7)));
    /// ```
    pub fn intersects_interval(&self, interval: &Interval<T>) -> bool {
        let (from, to) = Self::find_intersection_range(&self.intervals, interval);
        from < to
    }

    fn find_intersection_range(intervals: &Vec<Interval<T>>, interval: &Interval<T>) -> (usize, usize) {
        let intersect_from = Self::find_intersection_from_index(intervals, interval);
        let intersect_to = Self::find_intersection_to_index(intervals, interval);
        (intersect_from, intersect_to)
    }

    fn find_intersection_from_index(intervals: &Vec<Interval<T>>, interval: &Interval<T>) -> usize {
        let from_index = intervals.binary_search_by_key(
            &interval.get_lower_bound(),
            |probe| probe.get_upper_bound()
        );

        from_index.map(|value| value + 1).unwrap_or_else(|value| value)
    }

    fn find_intersection_to_index(intervals: &Vec<Interval<T>>, interval: &Interval<T>) -> usize {
        let to_index = intervals.binary_search_by_key(
            &interval.get_upper_bound(),
            |probe| probe.get_lower_bound()
        );

        to_index.map(|value| value.saturating_sub(1)).unwrap_or_else(|value| value)
    }

    /// Intersects `self` with the given [`Interval`] creating a new IntervalSet.
    ///
    /// [`Interval`]: ../interval/struct.Interval.html
    ///
    /// # Example
    /// ```
    /// # use interval_container_library::interval_set::IntervalSet;
    /// # use interval_container_library::interval::Interval;
    ///
    /// let mut interval_set = IntervalSet::new();
    /// interval_set.insert(Interval::new(0,2));
    /// interval_set.insert(Interval::new(7,10));
    ///
    /// assert_eq!(
    ///     IntervalSet::from(vec![Interval::new(1,2), Interval::new(7,8)]),
    ///     interval_set.intersection_interval(&Interval::new(1,8))
    /// );
    /// assert_eq!(IntervalSet::new(), interval_set.intersection_interval(&Interval::new(2,7)));
    /// ```
    pub fn intersection_interval(&self, interval: &Interval<T>) -> IntervalSet<T>
    where
        T: Clone
    {
        let (from, to) = Self::find_intersection_range(&self.intervals, interval);
        let intersecting_elements = Self::subset_iterator(&self.intervals, from, to);
        Self::copy_intersecting_elements(interval, intersecting_elements)
    }

    fn subset_iterator(intervals: &Vec<Interval<T>>, from: usize, to: usize) -> Option<impl Iterator<Item=&Interval<T>> + '_> {
        let num_intervals = to.saturating_sub(from);
        if from < intervals.len() &&
            0 < num_intervals {
            Some(intervals.iter().skip(from).take(num_intervals))
        } else {
            None
        }
    }

    fn copy_intersecting_elements<'a, I>(interval: &'a Interval<T>, iterator: Option<I>) -> Self
    where
        T: Clone,
        I: Iterator<Item=&'a Interval<T>>
    {
        let iterator = match iterator {
            Some(iter) => iter,
            None => return IntervalSet::new(),
        };

        let intervals = iterator.map(|other| interval.intersection(other)).collect();

        IntervalSet {
            intervals
        }
    }
}

/// Creates an [`IntervalSet`] with the given intervals.
///
/// [`IntervalSet`]: interval_set/struct.IntervalSet.html
#[macro_export]
macro_rules! interval_set {
    ($($x:expr),*) => (
        IntervalSet::from(vec![$($x),*])
    );
    ($($x:expr,)*) => ($crate::interval_set![$($x),*])
}

impl<T: Clone> From<&[Interval<T>]> for IntervalSet<T>
where
    T: std::cmp::Ord + Clone
{
    fn from(slice: &[Interval<T>]) -> IntervalSet<T> {
        let mut interval_set = IntervalSet::with_capacity(slice.len());
        for interval in slice {
            interval_set.insert(interval.clone());
        }
        interval_set
    }
}

impl<T> From<Box<[Interval<T>]>> for IntervalSet<T>
where
    T: std::cmp::Ord + Clone
{
    fn from(boxed_slice: Box<[Interval<T>]>) -> IntervalSet<T> {
        Self::from(boxed_slice.into_vec())
    }
}

impl<T> From<Vec<Interval<T>>> for IntervalSet<T>
where
    T: std::cmp::Ord + Clone
{
    fn from(vec: Vec<Interval<T>>) -> IntervalSet<T> {
        let mut interval_set = IntervalSet::with_capacity(vec.len());
        vec
            .into_iter()
            .for_each(|interval| interval_set.insert(interval));
        interval_set
    }
}

impl<T> std::iter::FromIterator<Interval<T>> for IntervalSet<T>
where
    T: std::cmp::Ord + Clone,
{
    fn from_iter<I: IntoIterator<Item=Interval<T>>>(iter: I) -> IntervalSet<T> {
        let mut interval_set = IntervalSet::new();
        iter
            .into_iter()
            .for_each(
                |interval| interval_set.insert(interval)
        );
        interval_set
    }
}

impl<T> std::fmt::Display for IntervalSet<T>
where
    T: std::cmp::Ord + std::fmt::Display
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{")?;
        let mut iterator = self.iter();
        iterator.next().map_or_else(|| std::fmt::Result::Ok(()), |interval| write!(f, "{}", interval))?;
        iterator.try_fold((), |_, interval| write!(f, ", {}", interval))?;
        write!(f, "}}")
    }
}

#[cfg(test)]
mod test {
    use super::*;

    mod new {
        use super::*;

        #[test]
        fn new() {
            let interval_set = IntervalSet::<i32>::new();
            assert!(interval_set.is_empty());
        }
    }

    mod contains_value {
        use super::*;

        #[test]
        fn contained() {
            let interval_set = interval_set![
                Interval::new(0, 10)
            ];

            assert!(interval_set.contains_value(&0));
            assert!(interval_set.contains_value(&5));
            assert!(interval_set.contains_value(&9));
        }

        #[test]
        fn not_contained_before_first() {
            let interval_set = interval_set![
                Interval::new(0, 10),
                Interval::new(20, 30)
            ];

            assert!(!interval_set.contains_value(&-10));
            assert!(!interval_set.contains_value(&-1));
        }

        #[test]
        fn not_contained_between_two_intervals() {
            let interval_set = interval_set![
                Interval::new(0, 10),
                Interval::new(20, 30)
            ];

            assert!(!interval_set.contains_value(&10));
            assert!(!interval_set.contains_value(&15));
            assert!(!interval_set.contains_value(&19));
        }

        #[test]
        fn not_contained_after_last() {
            let interval_set = interval_set![
                Interval::new(0, 10),
                Interval::new(20, 30)
            ];

            assert!(!interval_set.contains_value(&30));
            assert!(!interval_set.contains_value(&35));
        }
    }

    mod contains_interval {
        use super::*;

        #[test]
        fn contained() {
            let interval_set = interval_set![
                Interval::new(0, 10)
            ];

            assert!(interval_set.contains_interval(&Interval::new(0, 10)));
            assert!(interval_set.contains_interval(&Interval::new(3, 7)));
        }

        #[test]
        fn not_contained_before_first() {
            let interval_set = interval_set![
                Interval::new(0, 10),
                Interval::new(20, 30)
            ];

            assert!(!interval_set.contains_interval(&Interval::new(-10, -5)));
            assert!(!interval_set.contains_interval(&Interval::new(-10, -0)));
        }

        #[test]
        fn not_contained_between_two_intervals() {
            let interval_set = interval_set![
                Interval::new(0, 10),
                Interval::new(20, 30)
            ];

            assert!(!interval_set.contains_interval(&Interval::new(10, 15)));
            assert!(!interval_set.contains_interval(&Interval::new(15, 19)));
        }

        #[test]
        fn not_contained_after_last() {
            let interval_set = interval_set![
                Interval::new(0, 10),
                Interval::new(20, 30)
            ];

            assert!(!interval_set.contains_interval(&Interval::new(30, 35)));
            assert!(!interval_set.contains_interval(&Interval::new(35, 40)));
        }

        #[test]
        fn not_contained_empty_interval() {
            let interval_set = interval_set![
                Interval::new(0, 10),
                Interval::new(20, 30)
            ];

            assert!(interval_set.contains_interval(&Interval::new(0, -10)));
            assert!(interval_set.contains_interval(&Interval::new(10, 0)));
            assert!(interval_set.contains_interval(&Interval::new(20, 10)));
            assert!(interval_set.contains_interval(&Interval::new(30, 20)));
            assert!(interval_set.contains_interval(&Interval::new(40, 30)));

            assert!(interval_set.contains_interval(&Interval::new(30, 0)));
            assert!(interval_set.contains_interval(&Interval::new(40, -10)));

        }
    }

    mod insert {
        use super::*;

        #[test]
        fn empty_into_empty() {
            let mut interval_set = IntervalSet::new();
            interval_set.insert(Interval::new(0, 0));

            assert!(interval_set.is_empty())
        }

        #[test]
        fn insert_before_first_non_abut() {
            let mut interval_set = interval_set![
                Interval::new(0, 10)
            ];

            interval_set.insert(Interval::new(-20, -10));

            let mut iterator = interval_set.iter();

            assert_eq!(Some(&Interval::new(-20, -10)), iterator.next());
            assert_eq!(Some(&Interval::new(0, 10)), iterator.next());
            assert_eq!(None, iterator.next());
        }

        #[test]
        fn insert_before_first_abuts() {
            let mut interval_set = interval_set![
                Interval::new(0, 10)
            ];

            interval_set.insert(Interval::new(-10, 0));

            let mut iterator = interval_set.iter();

            assert_eq!(Some(&Interval::new(-10, 10)), iterator.next());
            assert_eq!(None, iterator.next());
        }

        #[test]
        fn insert_before_first_overflaps() {
            let mut interval_set = interval_set![
                Interval::new(0, 10)
            ];

            interval_set.insert(Interval::new(-10, 5));

            let mut iterator = interval_set.iter();

            assert_eq!(Some(&Interval::new(-10, 10)), iterator.next());
            assert_eq!(None, iterator.next());
        }

        #[test]
        fn insert_subset() {
            let mut interval_set = interval_set![
                Interval::new(0, 10)
            ];

            interval_set.insert(Interval::new(0, 10));

            let mut iterator = interval_set.iter();

            assert_eq!(Some(&Interval::new(0, 10)), iterator.next());
            assert_eq!(None, iterator.next());
        }

        #[test]
        fn insert_empty_when_nonempty() {
            let mut interval_set = interval_set![
                Interval::new(0, 10)
            ];

            interval_set.insert(Interval::new(100, -100));

            let mut iterator = interval_set.iter();

            assert_eq!(Some(&Interval::new(0, 10)), iterator.next());
            assert_eq!(None, iterator.next());
        }

        #[test]
        fn insert_after_abut() {
            let mut interval_set = interval_set![
                Interval::new(0, 10)
            ];

            interval_set.insert(Interval::new(10, 20));

            let mut iterator = interval_set.iter();

            assert_eq!(Some(&Interval::new(0, 20)), iterator.next());
            assert_eq!(None, iterator.next());
        }

        #[test]
        fn insert_after_non_abut() {
            let mut interval_set = interval_set![
                Interval::new(0, 10)
            ];

            interval_set.insert(Interval::new(20, 30));

            let mut iterator = interval_set.iter();

            assert_eq!(Some(&Interval::new(0, 10)), iterator.next());
            assert_eq!(Some(&Interval::new(20, 30)), iterator.next());
            assert_eq!(None, iterator.next());
        }

        #[test]
        fn insert_in_between_non_abut() {
            let mut interval_set = interval_set![
                Interval::new(0, 10),
                Interval::new(40, 50),
            ];

            interval_set.insert(Interval::new(20, 30));

            let mut iterator = interval_set.iter();

            assert_eq!(Some(&Interval::new(0, 10)), iterator.next());
            assert_eq!(Some(&Interval::new(20, 30)), iterator.next());
            assert_eq!(Some(&Interval::new(40, 50)), iterator.next());
            assert_eq!(None, iterator.next());
        }

        #[test]
        fn insert_in_between_abut_both() {
            let mut interval_set = interval_set![
                Interval::new(0, 10),
                Interval::new(40, 50),
            ];

            interval_set.insert(Interval::new(10, 40));

            let mut iterator = interval_set.iter();

            assert_eq!(Some(&Interval::new(0, 50)), iterator.next());
            assert_eq!(None, iterator.next());
        }

        #[test]
        fn insert_overlapping_multiple_more_than_2() {
            let mut interval_set = interval_set![
                Interval::new(0, 10),
                Interval::new(40, 50),
                Interval::new(80, 90),
                Interval::new(120, 130),
            ];

            interval_set.insert(Interval::new(5, 125));

            let mut iterator = interval_set.iter();

            assert_eq!(Some(&Interval::new(0, 130)), iterator.next());
            assert_eq!(None, iterator.next());
        }

    }

    mod intersects_interval {
        use super::*;

        #[test]
        fn empty_interval_returns_false() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert!(!interval_set.intersects_interval(&Interval::new(10, -10)));
        }

        #[test]
        fn intersects_into() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert!(interval_set.intersects_interval(&Interval::new(-10, 5)));
        }

        #[test]
        fn intersects_out() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert!(interval_set.intersects_interval(&Interval::new(25, 40)));
        }

        #[test]
        fn intersects_in_between() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert!(interval_set.intersects_interval(&Interval::new(5, 25)));
        }

        #[test]
        fn not_intersects_in_between() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert!(!interval_set.intersects_interval(&Interval::new(12, 18)));
        }

        #[test]
        fn not_intersects_in_between_abuts() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert!(!interval_set.intersects_interval(&Interval::new(10, 20)));
        }

        #[test]
        fn not_intersects_before() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert!(!interval_set.intersects_interval(&Interval::new(-10, -5)));
        }

        #[test]
        fn not_intersects_before_abut() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert!(!interval_set.intersects_interval(&Interval::new(-10, 0)));
        }

        #[test]
        fn not_intersects_after() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert!(!interval_set.intersects_interval(&Interval::new(35, 40)));
        }

        #[test]
        fn not_intersects_abuts() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert!(!interval_set.intersects_interval(&Interval::new(30, 40)));
        }

    }

    mod intersection_interval {
        use super::*;

        #[test]
        fn empty_interval_returns_false() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert_eq!(interval_set![], interval_set.intersection_interval(&Interval::new(10, -10)));
        }

        #[test]
        fn intersection_into() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert_eq!(
                interval_set![Interval::new(0, 5)],
                interval_set.intersection_interval(&Interval::new(-10, 5))
            );
        }

        #[test]
        fn intersection_out() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert_eq!(
                interval_set![Interval::new(25, 30)],
                interval_set.intersection_interval(&Interval::new(25, 40))
            );
        }

        #[test]
        fn intersection_in_between() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert_eq!(
                interval_set![
                    Interval::new(5, 10),
                    Interval::new(20, 25)
                ],
                interval_set.intersection_interval(&Interval::new(5, 25))
            );
        }

        #[test]
        fn no_intersection_in_between() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert_eq!(
                interval_set![],
                interval_set.intersection_interval(&Interval::new(12, 18))
            );
        }

        #[test]
        fn no_intersection_in_between_abuts() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert_eq!(
                interval_set![],
                interval_set.intersection_interval(&Interval::new(10, 20))
            );
        }

        #[test]
        fn no_intersection_before() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert_eq!(
                interval_set![],
                interval_set.intersection_interval(&Interval::new(-10, -5))
            );
        }

        #[test]
        fn no_intersection_before_abut() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert_eq!(
                interval_set![],
                interval_set.intersection_interval(&Interval::new(-10, 0))
            );
        }

        #[test]
        fn no_intersection_after() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert_eq!(
                interval_set![],
                interval_set.intersection_interval(&Interval::new(35, 40))
            );
        }

        #[test]
        fn no_intersection_after_abuts() {
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 10),
                Interval::new(20, 30),
            ]);

            assert_eq!(
                interval_set![],
                interval_set.intersection_interval(&Interval::new(30, 40))
            );
        }
    }

    mod from_interval_slice {
        use super::*;

        #[test]
        fn from_interval_slice_empty() {
            let empty_vector : Vec<Interval<i32>> = Vec::new();
            let interval_set = IntervalSet::<i32>::from(empty_vector.as_slice());

            assert!(interval_set.is_empty());
        }

        #[test]
        fn from_interval_slice_with_values() {
            let i1 = Interval::new(0,1);
            let i2 = Interval::new(5,6);
            let interval_set = IntervalSet::from(vec![
                i2.clone(),
                i1.clone(),
            ].as_slice());

            let mut iter = interval_set.iter();
            assert_eq!(Some(&i1), iter.next());
            assert_eq!(Some(&i2), iter.next());
            assert_eq!(None, iter.next());
        }
    }

    mod from_boxed_interval_slice {
        use super::*;

        #[test]
        fn from_boxed_interval_slice_empty() {
            let empty_vector = Vec::new();
            let interval_set = IntervalSet::<i32>::from(empty_vector.into_boxed_slice());

            assert!(interval_set.is_empty());
        }

        #[test]
        fn from_boxed_interval_slice_with_values() {
            let i1 = Interval::new(0,1);
            let i2 = Interval::new(5,6);
            let interval_set = IntervalSet::from(vec![
                i2.clone(),
                i1.clone(),
            ].into_boxed_slice());

            let mut iter = interval_set.iter();
            assert_eq!(Some(&i1), iter.next());
            assert_eq!(Some(&i2), iter.next());
            assert_eq!(None, iter.next());
        }
    }

    mod from_interval_vec {
        use super::*;

        #[test]
        fn from_interval_vec_empty() {
            let empty_vector = Vec::new();
            let interval_set = IntervalSet::<i32>::from(empty_vector);

            assert!(interval_set.is_empty());
        }

        #[test]
        fn from_interval_vec_with_values() {
            let i1 = Interval::new(0,1);
            let i2 = Interval::new(5,6);
            let interval_set = IntervalSet::from(vec![
                i2.clone(),
                i1.clone(),
            ]);

            let mut iter = interval_set.iter();
            assert_eq!(Some(&i1), iter.next());
            assert_eq!(Some(&i2), iter.next());
            assert_eq!(None, iter.next());
        }
    }

    mod from_iterator {
        use super::*;

        #[test]
        fn from_iterator_empty_iterator_returns_empty_interval_set() {
            use std::iter::FromIterator;
            let interval_set = IntervalSet::<i32>::from_iter(Vec::new());

            assert!(interval_set.is_empty());
        }

        #[test]
        fn from_iterator_with_values() {
            use std::iter::FromIterator;
            let i1 = Interval::new(0,1);
            let i2 = Interval::new(5,6);
            let interval_set = IntervalSet::from_iter(vec![
                i2.clone(),
                i1.clone(),
            ]);

            let mut iter = interval_set.iter();
            assert_eq!(Some(&i1), iter.next());
            assert_eq!(Some(&i2), iter.next());
            assert_eq!(None, iter.next());
        }
    }

    mod interval_set_macro {
        use super::*;

        #[test]
        fn interval_set_macro_empty() {
            let interval_set : IntervalSet<i32> = interval_set![];
            assert!(interval_set.is_empty());
        }

        #[test]
        fn interval_set_with_values() {
            let i1 = Interval::new(0,1);
            let i2 = Interval::new(5,6);
            let interval_set = interval_set![
                Interval::new(5, 6),
                Interval::new(0, 1),
            ];

            let mut iter = interval_set.iter();
            assert_eq!(Some(&i1), iter.next());
            assert_eq!(Some(&i2), iter.next());
            assert_eq!(None, iter.next());
        }
    }

    mod display {
        use super::*;

        #[test]
        fn empty() {
            use std::fmt::Write as FmtWrite;
            let empty = IntervalSet::<i32>::new();
            let mut s = String::new();
            write!(&mut s, "{}", empty).unwrap();

            assert_eq!("{}", s);
        }

        #[test]
        fn nonempty() {
            use std::fmt::Write as FmtWrite;
            let interval_set = IntervalSet::from(vec![
                Interval::new(0, 1),
                Interval::new(5, 6)
            ]);
            let mut s = String::new();
            write!(&mut s, "{}", interval_set).unwrap();

            assert_eq!("{[0, 1), [5, 6)}", s);
        }

    }
}
